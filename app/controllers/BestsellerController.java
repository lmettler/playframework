package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.core.j.HttpExecutionContext;
import play.libs.Json;
import play.mvc.*;
import services.NYTimesService;
import javax.inject.Inject;
import java.util.concurrent.CompletionStage;


public class BestsellerController extends Controller {

    NYTimesService nyTimesService;

    @Inject
    public BestsellerController(NYTimesService nyTimesService) {
        this.nyTimesService = nyTimesService;
    }

    public CompletionStage<Result> bestseller () {
        return nyTimesService.bestseller().thenApplyAsync(bestseller -> ok(Json.toJson(bestseller)));
    }
}
