# Book schema

# --- !Ups

create table book (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    title varchar(255),
    isbn13 varchar(255),
    isbn10 varchar(255),
    description varchar(300),
    publisher varchar(300),
    pages int,
    PRIMARY KEY (id)
);

# --- !Downs

drop table book;