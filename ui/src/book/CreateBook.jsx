import React from "react";

class CreateBook extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: ""
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
        console.log(this.state.title);
    }

    //Post in Datenbank
   handleSubmit(event) {
        event.preventDefault();
        console.log(this.state);
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ title: this.state.title ,  isbn13: '1232161', isbn10: '23904', description: 'Erzeugtes Buch', pages: 3})
        };
        console.log(requestOptions.body);
        console.log(this.state.title);

        fetch("/api/books", requestOptions)
            .then(response => response.json())
            .then(data => this.setState({ title: data.title }));
    }


    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Title:
                    <input type="text" name="title" value={this.state.title}
                           onChange={this.handleInputChange}/>
                </label>
                <input type="submit" value="Create Book" />
            </form>
        );
    }
}

export default CreateBook;
