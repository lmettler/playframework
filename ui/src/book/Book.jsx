import React from "react";
import "./book.css";

class Book extends React.Component {

    render() {
        const { title, description, id} = this.props.book;
        return (
            <section className="book">
                <h1>{title}</h1>
                <p>{description}</p>
                <button className="deleteButton" onClick={this.props.deleteBook.bind(this, id)}>DELETE</button>
            </section>
        );
    }
}

export default Book;
