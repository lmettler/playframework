import React from "react";
import Book from "./Book.jsx";
import axios from 'axios';

class Books extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            books: []
        };
    }

    componentDidMount() {
        axios
            .get('/api/books')
            .then(res => this.setState({ books: res.data }));
    }

    /*componentDidMount() {
        fetch("/api/books")
            .then((res => {
                return res.json();
            }))
            .then(data => {
                this.setState({
                    books: data
                });
            });
    }
    */


    render() {
        return this.state.books.map((book) => (
            <Book key={book.id} book={book}  deleteBook={this.props.deleteBook}/>
            ));
    }
}

export default Books;
