import React from 'react';
import './App.css';
import logo from './book.svg';
import Books from './book/Books.jsx';
import CreateBook from './book/CreateBook.jsx';
import Home from "./Home.jsx";
import axios from 'axios';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from 'react-router-dom';

class App extends React.Component {

    deleteBook = (id)=> {
        axios.delete('/api/books/${id}').then(res =>
            this.setState({
                books: [...this.state.books.filter(book => book.id !== id)]
            })
        );
    };

    render() {
        return (
            <div className="App">
                <Router>
                    <header className="App-header">
                        <img src={logo} className="App-logo" alt="logo"/>
                        <p>Welcome to our Book store.</p>
                        <nav className="navbar">
                            <Link to="/">Home</Link> |
                            <Link to="/books">Books</Link> |
                            <Link to="/create">Create Book</Link>
                        </nav>
                    </header>
                    <Switch>
                        <Route path="/books">
                            <Books deleteBook={this.deleteBook}/>
                        </Route>
                        <Route path="/create">
                            <CreateBook/>
                        </Route>
                        <Route path="/Home">
                            <Home/>
                        </Route>
                    </Switch>
                </Router>
            </div>
        );
    }
}

export default App;